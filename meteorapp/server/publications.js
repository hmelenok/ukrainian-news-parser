Meteor.publish('posts', function (opts) {
  console.log(opts);
  var now = new Date();
  var opts = opts || {};
  var page = opts.page || 1;
  var search = (opts.filterTitle) ? {title: {$regex: opts.filterTitle, $options: 'i'}} : {}
  var res = Posts.find(search, {limit:page * 10, sort: {timestamp: -1}});
  console.log('posts in '+(new Date() - now)+'ms') ;
  return res;
});
