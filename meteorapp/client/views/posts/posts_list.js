Template.postsList.helpers({
  posts: function(argument) {
    Session.set('location', 'home');
    return Posts.find({}, {sort: {timestamp: -1}});
  },
  navdates: function(){
    return _.uniq(Posts.find({}, {
      sort: {
        pubDate: 1
      },
      fields: {
        pubDate: true
      }
    }).fetch().map(function(x) {
      return moment(x.pubDate).format('ddd, D MMM YYYY');
    }) , true);
  }
});

Template.postsList.events = {
  'click    .js-loadMore button': function () {
    loadMore({force: true});
    // console.log(Session.get('query'));
  }
}

function loadMore(opts) {
  var force = opts.force || false;
  var threshold, target = $('html');
  if (!target.length) return;

  threshold = $(window).scrollTop() + $(window).height() - target.height();

  // HACK: see http://www.meteorpedia.com/read/Infinite_Scrolling
  if (force || target.offset().top < threshold+1 && threshold < 2) {
    // console.log("OFF:"+ target.offset().top +" TR:"+  threshold +" ST:"+$(window).scrollTop() +" WH:"+ $(window).height() + ' FORSE:' + force);
    var query = Session.get('query');
    console.log(query);
    Session.set('query', { page:query.page+1})
  }
}

// init
Meteor.startup(function (argument) {
  Session.setDefault('query', {page:1});
  Session.setDefault('location', 'home');
  // console.log(Session.get('query'));
  $(window).scroll(loadMore);


})
