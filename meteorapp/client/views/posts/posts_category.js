Template.postsCategory.helpers({
  posts: function() {
    Session.set('location', this.category);
    return Posts.find({category: this.category}, {sort: {timestamp: -1}});
  }
});
