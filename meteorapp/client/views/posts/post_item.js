moment.locale('uk');
var count;
Template.postEven.helpers({
  domain: function() {
    var a = document.createElement('a');
    a.href = this.url;
    return a.hostname;
  },
  week_day_date: function(){
    var week_day_date_c = '';
    week_day_date_c = moment(this.pubDate).format('ddd, D MMM YYYY');
    return week_day_date_c;
  },
  hours_time: function(){
    var hours_time_c = '';
    hours_time_c = moment(this.pubDate).format('HH:mm:ss');
    return hours_time_c;
  }
});
Template.postOdd.helpers({
  domain: function() {
    var a = document.createElement('a');
    a.href = this.url;
    return a.hostname;
  },
  week_day_date: function(){
    var week_day_date_c = '';
    week_day_date_c = moment(this.pubDate).format('ddd, D MMM YYYY');
    return week_day_date_c;
  },
  hours_time: function(){
    var hours_time_c = '';
    hours_time_c = moment(this.pubDate).format('HH:mm:ss');
    return hours_time_c;
  }
  });
  Template.postItem.helpers({
    isEven: function () {
      if(!count){ count = 0;}
        count++;
        // console.log(count);
        return (count % 2) === 0;
      }
    });
