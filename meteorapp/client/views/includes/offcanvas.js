Template.offcanvas.helpers({
  category: function() {
    return _.uniq(Posts.find({}, {
      sort: {
        category: 1
      },
      fields: {
        category: true
      }
    }).fetch().map(function(x) {
      return x.category;
    }) , true);
  }
});
