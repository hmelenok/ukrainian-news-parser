Template.logoPlace.helpers({
  day: function() {
    return moment(new Date()).format('D');
  },
  month: function() {
    return moment(new Date()).format('MMM');
  },
  weekday: function() {
    return moment(new Date()).format('dddd');
  },
  year: function() {
    return moment(new Date()).format('YYYY');
  }
});
