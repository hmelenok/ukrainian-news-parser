# News Parsing Meteor App #

## Vagrant ##
1. in root

```
#!bash

vagrant up && vagrant ssh
```

## In ssh client ##

```
#!bash

cd /vagrant/meteorapp/
meteor
```

 if problems with mongo:

```
#!bash

meteor reset
```
(resetting collections of project)

## Meteor without vagrant(linux, unix) ##
in meteorapp dir

```
#!bash
meteor
```
